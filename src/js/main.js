const swiper1 = new Swiper('.why-buy-img-slider', {
  navigation: {
    nextEl: '.js-why-slider-button',
  },
  effect: 'fade',
  loop: true,
  fadeEffect: {
    crossFade: true
  },
  autoplay: {
    delay: 5000,
    reverseDirection: true
  },
});
const swiper2 = new Swiper('.why-buy-text-slider', {
  navigation: {
    nextEl: '.js-why-slider-button',
  },
  effect: 'fade',
  loop: true,
  fadeEffect: {
    crossFade: true
  },
  autoplay: {
    delay: 5000,
    reverseDirection: true
  },
});

if (document.querySelector('.reviews-slider-1')) {
  const swiper3 = new Swiper('.reviews-slider-1', {
    loop: true,
    slidesPerView: "auto",
    spaceBetween: 8,
    autoplay: {
      delay: 5000,
    },
  });
}

if (document.querySelector('.reviews-slider-2')) {
  const swiper4 = new Swiper('.reviews-slider-2', {
    loop: true,
    slidesPerView: "auto",
    spaceBetween: 8,
    autoplay: {
      delay: 5000,
      reverseDirection: true
    },
  });
}

//faq

for (const faqTitle of document.querySelectorAll('.faq-content-item__title')) {
  faqTitle.addEventListener('click', function () {
    const faqItem = this.closest('.faq-content-item');
    const heightText = faqItem.querySelector('.faq-content-item__text').scrollHeight;

    faqItem.classList.toggle('faq-content-item--active');

    if (faqItem.classList.contains('faq-content-item--active')) {
      faqItem.querySelector('.faq-content-item__text').style.maxHeight = heightText + 'px';
    } else {
      faqItem.querySelector('.faq-content-item__text').style.maxHeight = 0 + 'px';
    }
  });
}


let monthlyRevenue = document.querySelector('.js-monthly-revenue'),
  monthlyProfit = document.querySelector('.js-monthly-profit'),
  yearProfit = document.querySelector('.js-year-profit');

monthlyRevenue.textContent = new Intl.NumberFormat('ru-RU').format(monthlyRevenue.getAttribute('data-price'));
monthlyProfit.textContent = new Intl.NumberFormat('ru-RU').format(monthlyProfit.getAttribute('data-price'));
yearProfit.textContent = new Intl.NumberFormat('ru-RU').format(yearProfit.getAttribute('data-price'));


$(function () {
  var handle = $("#custom-handle");
  $("#slider").slider({
    min: 0,
    max: 6,
    value: 1,
    create: function () {
      handle.text($(this).slider("value"));
    },
    slide: function (event, ui) {
      handle.text(ui.value);
      let monthlyRevenueText = Number(monthlyRevenue.getAttribute('data-price')) * ui.value,
        monthlyProfitText = Number(monthlyProfit.getAttribute('data-price')) * ui.value,
        yearProfitText = 12 * monthlyProfitText;
      monthlyRevenue.textContent = new Intl.NumberFormat('ru-RU').format(monthlyRevenueText);
      monthlyProfit.textContent = new Intl.NumberFormat('ru-RU').format(monthlyProfitText);
      yearProfit.textContent = new Intl.NumberFormat('ru-RU').format(yearProfitText);
    }
  });
});

//плавный скролл
$('.scroll-link').on('click', function () {
  let href = $(this).attr('href');

  $('.header-nav').removeClass('header-nav--active');
  $('.hamburger').removeClass('hamburger--active')


  $('html, body').animate({
    scrollTop: $(href).offset().top
  }, {
    duration: 370,
    easing: "linear"
  });

  return false;
});


//отправка формы

const ajaxSend = async (formData, action) => {
  const fetchResp = await fetch(action, {
    method: 'POST',
    body: formData
  });
  if (!fetchResp.ok) {
    throw new Error(`Ошибка по адресу ${url}, статус ошибки ${fetchResp.status}`);
  }
  return await fetchResp.json();
};

const forms = document.querySelectorAll('form');

forms.forEach(form => {
  form.addEventListener('submit', function (e) {
    e.preventDefault();
    const formData = new FormData(this);
    const action = form.getAttribute('action');

    ajaxSend(formData, action)
      .then((response) => {
        if (response.status) {
          form.reset(); // очищаем поля формы
          document.querySelector('.popup-thankyou').classList.add('d-block');
        } else {
          alert('Ошибка при отправке формы');
        }
      })
      .catch((err) => {
        alert('Ошибка при отправке формы');
      });
  });
});


//закрытие popup

for (const closePopup of document.querySelectorAll('.js-popup-close')) {
  closePopup.addEventListener('click', function () {
    this.closest('.popup-thankyou').classList.remove('d-block');
  });

}


//hamburger
const hamburger = document.querySelector('.hamburger');

hamburger.addEventListener('click', function () {
  this.classList.toggle('hamburger--active');
  document.querySelector('.header-nav').classList.toggle('header-nav--active');
})


//carousel

var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("work-experience-bg");
  for (i = 0; i < x.length; i++) {
    x[i].classList.remove('work-experience-bg--active');
  }
  myIndex++;
  if (myIndex > x.length) {
    myIndex = 1
  }
  x[myIndex - 1].classList.add('work-experience-bg--active');
  setTimeout(carousel, 6400); // Change image every 5 seconds
}


//mask-phone
$("#phone").mask("+7(999) 999-99-99");