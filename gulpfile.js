'use strict';
const {  src, dest, watch, series, parallel } = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var fileinclude = require('gulp-file-include');

function html() {
  return src('src/**/*.html')
    .pipe(fileinclude())
    .pipe(dest('build'))
    .pipe(browserSync.reload({stream: true}));
}

function fileinclude() {
  return src('src/**/*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
      indent: true
    }));
}

function fonts() {
  return src('src/fonts/**/*.*')
    .pipe(dest('build/fonts'))
    .pipe(browserSync.reload({stream: true}));
}

function css() {
  return src('src/style/**/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({
        overrideBrowserslist:  ['last 2 versions'],
        cascade: false
    }))
    //.pipe(minifyCSS())
    .pipe(dest('build/css'))
    .pipe(browserSync.reload({stream: true}));
}

function js() {
  return src('src/js/**/*.js', { sourcemaps: false })
    //.pipe(uglify())
    .pipe(dest('build/js', { sourcemaps: false }))
    .pipe(browserSync.reload({stream: true}));
}

function img() {
  return src('src/img/**/*.*')
    .pipe(dest('build/img'))
    .pipe(browserSync.reload({stream: true}));
}

function watchFilesSCSS() {
    watch(
      ['./src/style/*.scss', './src/style/**/*.scss'],
      { events: 'all', ignoreInitial: false },
      series(css)
    );
}

function watchFilesHTML() {
  watch(
    ['./src/*.html', './src/**/*.html'],
    { events: 'all', ignoreInitial: false },
    series(html)
  );
}

function watchFilesJS() {
  watch(
    ['./src/js/**/*.js'],
    { events: 'all', ignoreInitial: false },
    series(js)
  );
}

function watchFilesImages() {
  watch(
    ['./src/img/**/*.*'],
    { events: 'all', ignoreInitial: false },
    series(img)
  );
}

function watchFilesFonts() {
  watch(
    ['./src/fonts/*.*'],
    { events: 'all', ignoreInitial: false },
    series(fonts)
  );
}

function serve(done) {
  browserSync.init({
    server: {
      baseDir: "./build"
    }
  });
  done();
}

exports.js = js;
exports.css = css;
exports.html = html;
exports.img = img;
exports.fonts = fonts;
exports.default = parallel(html, css, js, img, fonts);
exports.serve = parallel(serve, img, fonts, css, js, html, watchFilesImages, watchFilesFonts, watchFilesSCSS, watchFilesJS, watchFilesHTML);